package components.chat

import androidx.compose.foundation.layout.Box
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import server.Message

@Composable
inline fun ChatMessage(msg: Message) {
    Box {
        Text("${msg.username}: ${msg.text}")
    }
}
