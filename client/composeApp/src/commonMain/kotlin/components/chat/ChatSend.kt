package components.chat

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.heightIn
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.Icon
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Send
import androidx.compose.runtime.Composable
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.input.key.*
import androidx.compose.ui.unit.dp

@Composable
inline fun ChatSend(crossinline sendMessage: (String) -> Unit) {
    var msg by remember { mutableStateOf("") }
    val focus = remember { FocusRequester() }

    val sendAction: () -> Unit = {
        sendMessage(msg)
        msg = ""
    }

    TextField(
        modifier = Modifier
            .fillMaxWidth()
            .heightIn(max = 120.dp)
            .focusRequester(focus)
            .onKeyEvent {
                if (!it.isShiftPressed && it.key == Key.Enter) {
                    msg = msg.dropLast(1) // drop newline character
                    sendAction()
                    true
                } else if (it.isShiftPressed && it.key == Key.Enter && it.type == KeyEventType.KeyDown) {
                    msg += "\n"
                    true
                } else {
                    false
                }
            },
        label = { Text("Type a cool message...") },
        value = msg,
        onValueChange = { msg = it },
        trailingIcon = {
            if (msg.isNotEmpty()) {
                Icon(
                    imageVector = Icons.Default.Send,
                    contentDescription = "Send",
                    modifier = Modifier.clickable {
                        sendAction()
                        focus.requestFocus()
                    }
                )
            }
        }
    )
}
