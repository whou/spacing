package screen

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.currentOrThrow
import components.chat.ChatMessage
import components.chat.ChatMessagesLog
import components.chat.ChatSend
import kotlinx.coroutines.*
import server.Message

import server.ServerClient

data class ChatScreen(val serverClient: ServerClient) : Screen {

    private val session = serverClient.listenToServer()

    @Composable
    override fun Content() {
        val navigator = LocalNavigator.currentOrThrow

        Scaffold(
            topBar = {
                TopAppBar {
                    IconButton(
                        enabled = navigator.canPop,
                        onClick = {
                            runBlocking { session.close() }
                            serverClient.close()
                            navigator.pop()
                        }
                    ) {
                        Icon(
                            imageVector = Icons.Default.ArrowBack,
                            contentDescription = "Quit chat"
                        )
                    }
                }
            }
        ) {
            Column(
                Modifier.fillMaxSize(),
            ) {
                val messages = remember { mutableStateListOf<Message>() }

                Box(
                    Modifier.weight(1f)
                ) {
                    ChatMessagesLog(messages)
                }

                ChatSend {
                    runBlocking { session.send(it) }
                    messages.add(Message(it, "me"))
                }
            }
        }
    }
}