package screen

import server.ServerClient

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.style.TextAlign
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.currentOrThrow
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import server.ServerErrorException
import server.User

const val DEFAULT_URL = "http://127.0.0.1:4040"

class LoginScreen : Screen {

    @Composable
    override fun Content() {
        var connecting by remember { mutableStateOf(false) }
        var connected by remember { mutableStateOf(false) }
        var signup by remember { mutableStateOf(false) }
        var url by remember { mutableStateOf(DEFAULT_URL) }
        var error: String? by remember { mutableStateOf(null) }
        var client: ServerClient? by remember { mutableStateOf(null) }

        val navigator = LocalNavigator.currentOrThrow
        val scope = rememberCoroutineScope()

        val connectToServer: () -> Unit = {
            connecting = true
            error = null
            scope.launch {
                client = ServerClient(url)
                connected = try {
                    coroutineScope { client?.getInfo() }
                    true
                } catch (e: ServerErrorException) {
                    error = "${e.message} (${e.status})"
                    false
                } catch (e: Exception) {
                    error = e.message
                    false
                }
            }
        }

        Column(
            Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceBetween
        ) {
            Column(
                Modifier
                    .fillMaxHeight()
                    .weight(1f, false)
                    .verticalScroll(rememberScrollState()),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                OutlinedTextField(
                    label = { Text("Server URL") },
                    value = url,
                    onValueChange = { url = it },
                    singleLine = true,
                    enabled = !connected,
                    keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                    keyboardActions = KeyboardActions(
                        onDone = { connectToServer() }
                    )
                )

                if (!connected) {
                    Button(onClick = { connectToServer() }) {
                        Text("Connect to the server")
                    }

                    AnimatedVisibility(connecting) {
                        if (error == null) {
                            Text("Connecting...")
                        } else {
                            Text(
                                "Connection Failed:\n$error",
                                color = Color.Red,
                                fontWeight = FontWeight.Bold,
                                textAlign = TextAlign.Center
                            )

                            client?.close()
                            client = null
                        }
                    }
                } else {
                    AnimatedVisibility(connected) {
                        var username by rememberSaveable { mutableStateOf("") }
                        var password by rememberSaveable { mutableStateOf("") }
                        var loggingIn by remember { mutableStateOf(false) }
                        var authenticated by remember { mutableStateOf(false) }
                        val passwordFocus = remember { FocusRequester() }

                        val loginToServer: () -> Unit = {
                            loggingIn = true
                            error = null
                            client?.user = User(username, password)
                            scope.launch {
                                authenticated = try {
                                    coroutineScope { client!!.getAuth() }
                                } catch (e: Exception) {
                                    error = e.message
                                    false
                                }

                                if (!authenticated && error == null) {
                                    error = "Incorrect username or password"
                                }
                            }
                        }

                        val signupToServer: () -> Unit = {
                            loggingIn = true
                            error = null
                            client?.user = User(username, password)
                            scope.launch {
                                authenticated = try {
                                    coroutineScope { client!!.signupUser() }
                                } catch (e: Exception) {
                                    error = e.message
                                    false
                                }
                            }
                        }

                        Column(
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) {
                            OutlinedTextField(
                                label = { Text("Username") },
                                value = username,
                                onValueChange = { username = it },
                                singleLine = true,
                                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                                keyboardActions = KeyboardActions(
                                    onDone = { passwordFocus.requestFocus() }
                                )
                            )

                            OutlinedTextField(
                                modifier = Modifier.focusRequester(passwordFocus),
                                label = { Text("Password") },
                                value = password,
                                onValueChange = { password = it },
                                singleLine = true,
                                visualTransformation = PasswordVisualTransformation(),
                                keyboardOptions = KeyboardOptions(
                                    keyboardType = KeyboardType.Password,
                                    imeAction = ImeAction.Done
                                ),
                                keyboardActions = KeyboardActions(
                                    onDone = {
                                        if (signup) {
                                            signupToServer()
                                        } else {
                                            loginToServer()
                                        }
                                    }
                                )
                            )

                            if (signup) {
                                Button(onClick = { signupToServer() }) {
                                    Text("Sign up")
                                }
                            } else {
                                Button(onClick = { loginToServer() }) {
                                    Text("Login")
                                }
                            }

                            AnimatedVisibility(loggingIn) {
                                if (error == null) {
                                    Text(
                                        if (signup) {
                                            "Creating account..."
                                        } else {
                                            "Logging in..."
                                        }
                                    )
                                } else {
                                    Text(
                                        error!!,
                                        color = Color.Red,
                                        fontWeight = FontWeight.Bold,
                                        textAlign = TextAlign.Center
                                    )
                                }
                            }

                            if (authenticated) {
                                navigator.push(ChatScreen(client!!))
                            }
                        }
                    }
                }
            }

            if (connected) {
                TextButton(onClick = {
                    signup = !signup
                    error = null
                }) {
                    Text(
                        if (signup) {
                            "Login to account"
                        } else {
                            "Create account"
                        }
                    )
                }
            }
        }
    }
}
