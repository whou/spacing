package server

import kotlinx.serialization.Serializable

@Serializable
data class ServerInfo(val version: String)
