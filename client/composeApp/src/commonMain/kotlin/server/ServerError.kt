package server

import kotlinx.serialization.Serializable

@Serializable
data class ServerError(val error: String)
