package server

data class ServerErrorException(
    val status: Int,
    override val message: String
) : Exception(message)

class FailedConnectionException(url: String, error: String)
    : Exception("Connection to \"$url\" failed. The server might be offline. ($error)")

class InvalidServerException(url: String)
    : Exception("Server on \"$url\" is invalid. It might not be a Spacing Chat server.")
