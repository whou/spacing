package server

import io.ktor.client.*
import io.ktor.client.plugins.websocket.*
import io.ktor.client.request.*
import io.ktor.websocket.*
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.runBlocking

class SocketSession(
    private val client: HttpClient,
    private val path: String,
    private val host: String,
    private val port: Int,
    private val request: HttpRequestBuilder.() -> Unit = {}
) {

    private val session = runBlocking {
        client.webSocketSession(
            path = path,
            host = host,
            port = port,
            block = request
        )
    }

    // throws `Exception` on fail
    suspend fun send(message: String) {
        session.outgoing.send(Frame.Text(message))
    }

    suspend fun receive(onReceive: (String) -> Unit) {
        client.webSocket(
            path = path,
            host = host,
            port = port,
            request = request
        ) {
            session.incoming.consumeEach { frame ->
                if (frame is Frame.Text) {
                    onReceive(frame.readText())
                }
            }
        }
    }

    suspend fun close() {
        session.close()
    }
}