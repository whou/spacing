package server

data class Message(val text: String, val username: String)
