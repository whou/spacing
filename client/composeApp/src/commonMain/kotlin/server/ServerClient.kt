package server

import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.websocket.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.utils.io.core.*
import io.ktor.utils.io.errors.*
import kotlinx.serialization.json.internal.writeJson
import kotlin.io.encoding.Base64
import kotlin.io.encoding.ExperimentalEncodingApi

class ServerClient(private val server: String) {
    private val client = HttpClient(CIO) {
        install(WebSockets)
        install(ContentNegotiation) {
            json()
        }
    }

    var user: User? = null

    suspend fun getInfo(): ServerInfo {
        val response = try {
            client.get(server)
        } catch (e: IOException) {
            throw FailedConnectionException(server, e.message!!)
        }

        if (response.status.value != 200) {
            throw ServerErrorException(response.status.value, response.status.description)
        }

        return  try {
            response.body<ServerInfo>()
        } catch (e: NoTransformationFoundException) {
            throw InvalidServerException(server)
        }
    }

    suspend fun getAuth(): Boolean {
        val response = client.request("$server/auth") {
            method = HttpMethod.Get
            headers {
                buildAuthHeader()
            }
        }

        return when (response.status.value) {
            401 -> false
            200 -> true
            else -> throw ServerErrorException(response.status.value, response.status.description)
        }
    }

    suspend fun signupUser(): Boolean {
        val response = client.request("$server/signup")  {
            method = HttpMethod.Post
            contentType(ContentType.Application.Json)
            setBody(user!!)
        }

        return when (response.status.value) {
            200 -> true
            else -> throw ServerErrorException(response.status.value, response.body<ServerError>().error)
        }
    }

    fun listenToServer(): SocketSession {
        val address = server.substringAfter("://").split(":", limit = 2)
        return SocketSession(
            client = client,
            path = "/ws",
            host = address[0],
            port = address[1].toInt(),
            request = {
                buildAuthHeader()
            }
        )
    }

    fun close() {
        client.close()
    }

    @OptIn(ExperimentalEncodingApi::class)
    private val buildAuthHeader: HttpMessageBuilder.() -> Unit = {
        if (user == null) {
            throw Exception("User is not assigned")
        }
        header("Authorization", "Basic ${Base64.encode("${user?.username}:${user?.password}".toByteArray())}")
    }
}
