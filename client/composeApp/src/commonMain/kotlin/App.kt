import androidx.compose.material.*
import androidx.compose.runtime.Composable
import cafe.adriel.voyager.navigator.Navigator
import cafe.adriel.voyager.transitions.SlideTransition

import screen.LoginScreen

@Composable
fun App() {
    MaterialTheme {
        Navigator(LoginScreen()) {
            SlideTransition(it)
        }
    }
}