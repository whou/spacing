import 'dart:convert';
import 'dart:io';

import 'package:pocketbase/pocketbase.dart';
import 'package:spacing/server/spacing.dart';
import 'package:spacing/server/version.dart';

// server status response
void indexRoute(HttpRequest req) async {
  const payload = {'version': serverVersion};
  var res = req.response;
  res.headers.contentType =
      ContentType.parse('application/json; charset=utf-8');
  res.statusCode = HttpStatus.ok;
  res.write(jsonEncode(payload));
  res.close();
}

void signupRoute(HttpRequest req, final PocketBase pb) async {
  var res = req.response;

  if (req.method != 'POST') {
    res.statusCode = HttpStatus.methodNotAllowed;
    res.close();
    return;
  }

  final body = await utf8.decodeStream(req);
  if (body.isEmpty) {
    res.statusCode = HttpStatus.badRequest;
    res.close();
    return;
  }

  final dynamic data;
  try {
    data = jsonDecode(body);
  } on FormatException {
    res.statusCode = HttpStatus.badRequest;
    res.close();
    return;
  }

  final String? username = data['username'];
  final String? password = data['password'];
  if (username == null || password == null) {
    res.statusCode = HttpStatus.badRequest;
    res.close();
    return;
  }

  try {
    await pb.collection('users').create(body: {
      'username': username,
      'password': password,
      'passwordConfirm': password
    });
  } on ClientException catch (e) {
    final err = e.response['data'];
    if (err != null) {
      res.headers.contentType =
          ContentType.parse('application/json; charset=utf-8');
      res.statusCode = HttpStatus.conflict;
      if (err['username'] != null) {
        res.write({'error': '"${err['username']['message']}"'});
        res.close();
        return;
      }
      if (err['password'] != null) {
        res.write({'error': '"${err['password']['message']}"'});
        res.close();
        return;
      }
    }

    rethrow;
  }

  res.statusCode = HttpStatus.ok;
  res.close();
  return;
}

void authRoute(HttpRequest req, final PocketBase pb) async {
  var res = req.response;

  final auth = req.headers.value(HttpHeaders.authorizationHeader);
  final user = decodeAuthHeader(auth);

  // no authentication header, return 400 unauthorized
  if (user == null) {
    res.statusCode = HttpStatus.badRequest;
    res.close();
    return;
  }

  if (!await user.isAuthorized(pb)) {
    res.statusCode = HttpStatus.unauthorized;
    res.close();
    return;
  }

  res.statusCode = HttpStatus.ok;
  res.close();
}

void notFoundRoute(HttpRequest req) async {
  var res = req.response;
  res.statusCode = 404;
  res.close();
}
