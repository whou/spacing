import 'dart:convert';
import 'dart:io';

import 'package:pocketbase/pocketbase.dart';
import 'package:spacing/server/routes.dart';
import 'package:spacing/server/user.dart';

void runServer(HttpServer server, final String dbAddress) async {
  final pb = PocketBase(dbAddress);
  await pb.health.check().onError<ClientException>((e, _) {
    print('Unable to connect to PocketBase server at ${pb.baseUrl}');
    throw e;
  });

  void closeServer(_) {
    print('Closing the server...');
    exit(0);
  }

  ProcessSignal.sigint.watch().listen(closeServer);
  ProcessSignal.sigterm.watch().listen(closeServer);

  print('Connected to PocketBase server on ${pb.baseUrl}');
  print('Close the server with CTRL-C');
  print('Server running on http://${server.address.host}:${server.port}');
  await for (HttpRequest req in server) {
    _processRequest(req, pb);
  }
}

void _processRequest(HttpRequest req, final PocketBase pb) async {
  switch (req.uri.path) {
    case '/':
      indexRoute(req);
      break;
    case '/signup':
      signupRoute(req, pb);
      break;
    case '/auth':
      authRoute(req, pb);
      break;
    case '/ws':
      _handleWS(req, pb);
      break;
    default:
      notFoundRoute(req);
  }
}

void _handleWS(HttpRequest req, final PocketBase pb) async {
  final auth = req.headers.value(HttpHeaders.authorizationHeader);
  final user = decodeAuthHeader(auth);
  if (user == null) {
    var res = req.response;
    res.statusCode = HttpStatus.badRequest;
    res.close();
    return;
  }

  if (!await user.isAuthorized(pb)) {
    var res = req.response;
    res.statusCode = HttpStatus.unauthorized;
    res.close();
    return;
  }

  final username = user.username;

  print('Client $username connected.');

  // upgrade the incoming request to a WebSocket connection
  var socket = await WebSocketTransformer.upgrade(req);
  socket.listen(
    (message) {
      print('Received message from $username: $message');
    },
    onDone: () => print('Client $username disconnected.'),
  );
}

User? decodeAuthHeader(final String? header) {
  if (header == null || header.isEmpty || !header.startsWith('Basic ')) {
    return null;
  }

  final encoded = header.replaceFirst('Basic ', '');
  final decoded = utf8.decode(base64.decode(encoded));
  if (!decoded.contains(':')) {
    return null;
  }

  final credentials = decoded.split(':');
  credentials.length = 2;

  return User(credentials.first, credentials.last);
}
