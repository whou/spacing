import 'package:pocketbase/pocketbase.dart';

final class User {
  final String username;
  final String password;

  const User(this.username, this.password);

  Future<bool> isAuthorized(final PocketBase pb) async {
    try {
      await pb.collection('users').authWithPassword(username, password);
    } on ClientException catch (e) {
      if (e.statusCode == 400) {
        return false;
      }
      rethrow;
    }

    final valid = pb.authStore.isValid;
    pb.authStore.clear();
    return valid;
  }
}
