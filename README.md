# Spacing Chat

A try at a chat app.

## Server
The Spacing Chat server requires a running [PocketBase](https://pocketbase.io) server.

Run the Spacing Chat server with Dart using
```
dart run
```

## Client
The client is made with Kotlin Compose. Make sure you have JDK 21 installed and run inside the `client` directory:
```
./gradlew run
```

## License
The Spacing Chat server is licensed under [AGPL 3.0](https://www.gnu.org/licenses/agpl-3.0.html) or later. You may freely copy, distribute and modify it. Any modifications, including on network use, must also be distributed under AGPL. You can read the [COPYING](./COPYING) file for more information.

The Spacing Chat client is licensed under [GPL 3.0](https://www.gnu.org/licenses/gpl-3.0.html) or later. You may freely copy, distribute and modify it. Any modifications must also be distributed under GPL. You can read the [COPYING](./client/COPYING) file for more information.
