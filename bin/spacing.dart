import 'dart:io';

import 'package:dotenv/dotenv.dart';
import 'package:spacing/server/spacing.dart';

void main(List<String> arguments) async {
  // load server environment variables
  if (!await File.fromUri(Uri.file('.env')).exists()) {
    throw Exception('.env file does not exist');
  }
  final env = DotEnv()..load();

  if (!env.isDefined('PB_ADDRESS')) {
    throw Exception(
        '.env file is missing the PocketBase server address as the PB_ADDRESS variable');
  }

  var server = await HttpServer.bind(InternetAddress.loopbackIPv4, 4040);
  runServer(server, env['PB_ADDRESS']!);
}
